import pygame, sys, random
from pygame.locals import *
# Create the constants (go ahead and experiment with different values)
BOARDWIDTH = 5 # number of columns in the board
BOARDHEIGHT = 5 # number of rows in the board
TILESIZE = 80
WINDOWWIDTH = 800
WINDOWHEIGHT = 600
FPS = 30
BLANK = None
#                 R    G    B
BLACK =         (  0,   0,   0)
WHITE =         (255, 255, 255)
CORAL =         (255,  137, 118)
PEACH =         (255,  229,  180)
GREEN =         (  0, 128,   0)
RED =           (255, 0, 0)
BGCOLOR =   PEACH
TILECOLOR = CORAL
TEXTCOLOR = WHITE
BORDERCOLOR = RED
BASICFONTSIZE = 20
TEXT = GREEN
BUTTONCOLOR = WHITE
BUTTONTEXTCOLOR = BLACK
MESSAGECOLOR = BLACK
XMARGIN = int((WINDOWWIDTH - (TILESIZE * BOARDWIDTH + (BOARDWIDTH - 1))) / 2)
YMARGIN = int((WINDOWHEIGHT - (TILESIZE * BOARDHEIGHT + (BOARDHEIGHT - 1))) / 2)
UP = 'up'
DOWN = 'down'
LEFT = 'left'
RIGHT = 'right'
